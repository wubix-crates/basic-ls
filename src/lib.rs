use clap::Clap;
use librust::{
    eprintln,
    ffi::OsString,
    fs::read_dir,
    path::{Path, PathBuf},
    println,
    process::exit,
    stream::{iter, StreamExt},
};
use std::future::ready;
use wasm_bindgen::prelude::*;

/// List files and directory contents. A basic implementation of `ls` from `coreutils`
#[derive(Debug, Clap)]
#[clap(version = clap::crate_version!())]
struct Arguments {
    /// Print all files, including `.` and `..`
    #[clap(long, short = 'a')]
    all: bool,
    /// Print all files, except `.` and `..`
    #[clap(long, short = 'A')]
    almost_all: bool,
    /// Do not print files ending with ~
    #[clap(long, short = 'B')]
    ignore_backups: bool,
    /// List directories as files
    #[clap(long, short = 'd')]
    directory: bool,
    /// Files and directories to list
    files: Vec<PathBuf>,
}

struct Directory {
    path: PathBuf,
    files: Vec<OsString>,
}

impl Directory {
    async fn show(&self) {
        for file in &self.files {
            println!("{}", Path::new(file).display()).await;
        }
    }
}

#[wasm_bindgen]
pub async fn start() {
    let mut arguments = Arguments::parse().await;

    if arguments.files.is_empty() {
        arguments.files.push(PathBuf::from("."));
    }

    let queried_files = arguments.files.len();
    let mut exit_code = 0;
    let mut files: Vec<PathBuf> = Vec::new();
    let mut directories = Vec::new();

    for path in std::mem::take(&mut arguments.files) {
        match read_dir(&path).await {
            Ok(_) if arguments.directory => files.push(path),
            Ok(directory) => directories.push(Directory {
                path,
                files: iter(if arguments.all && !arguments.almost_all {
                    vec![OsString::from("."), OsString::from("..")]
                } else {
                    Vec::new()
                })
                .chain(
                    directory
                        .filter_map(|file| ready(file.ok().map(|file| file.file_name())))
                        .filter(|name| {
                            let is_backup =
                                arguments.ignore_backups && name.as_bytes().ends_with(b"~");
                            let is_hidden = !(arguments.almost_all || arguments.all)
                                && name.as_bytes().starts_with(b".");

                            ready(!is_backup && !is_hidden)
                        }),
                )
                .collect()
                .await,
            }),
            Err(error) if error.to_string() == "Not a directory" => files.push(path),
            Err(error) => {
                exit_code = 1;
                eprintln!("ls: cannot access '{}': {}", path.display(), error).await;
            }
        }
    }

    for file in &files {
        println!("{}", file.display()).await;
    }

    if queried_files == 1 && !directories.is_empty() {
        directories[0].show().await;
    } else {
        let mut needs_new_line = !files.is_empty();
        for directory in directories {
            if needs_new_line {
                println!().await;
            }
            println!("{}:", directory.path.display()).await;
            directory.show().await;
            needs_new_line = true;
        }
    }

    exit(exit_code);
}
